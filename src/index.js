
    const TODO_API_ENDPOINT = 'https://jsonplaceholder.typicode.com/todos';
    let toDoList;
    let tempToDoList = [];
    let userList = new Set();
    let sortOrder = {
        id:false,
        title:false,
        completed: false
    }

    
    document.addEventListener("DOMContentLoaded", function (event) {
        loadToDoList();
    });

    function loadToDoList() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xmlhttp.status == 200) {
                toDoList = JSON.parse(xmlhttp.responseText);
                renderTableHeader();
                renderTable();
                setUserIdSelect();
            } else if (xmlhttp.status == 400) {
                alert('There was an error 400');
            } else {
                alert('something else other than 200 was returned');
            }
            }
        };
        xmlhttp.open("GET", TODO_API_ENDPOINT, true);
        xmlhttp.send();
    }
    
    function setUserIdSelect() {
        let userSelect = document.getElementById('user-id-selector');
        for (let userId of userList) userSelect.innerHTML += `<option>${userId}</option>`;
    }

    function renderTable(userId = null,sortBy=null) {
        let table = document.getElementById("todo-table");
        let tBody = document.createElement('tbody');
        if(table.childNodes[2] === undefined) {
            table.appendChild(tBody);
        } else {
             tBody =table.childNodes[2];
             tBody.innerHTML = "";
        }

        let row;
        if(userId == null) {
            tempToDoList = toDoList;
        } else {
            tempToDoList = toDoList.filter((todo) => todo.userId == userId);
        }
        /* Sorting logic */
        sort(sortBy);
        
       
     
        tempToDoList.map((todo,index) => {
            userList.add(todo.userId);
            row = tBody.insertRow(index );
            row.insertCell(0).innerHTML = todo.id;
            row.insertCell(1).innerHTML = todo.title;
            checked = todo.completed ? 'checked' : '';
            row.insertCell(2).innerHTML = `<input type="checkbox" ${checked} disabled >`;
        });
        drawChart();
    }
    function sort(sortBy) {
        switch(sortBy) {
            case 'id':
            tempToDoList.sort(function(a, b) {
                if(sortOrder.id) {
                    return a.id > b.id;
                }
                return a.id < b.id;
            });
            sortOrder.id = !sortOrder.id;
            break;
            case 'title':
                tempToDoList.sort(function(a, b) {
                     if(sortOrder.title) {
                        return b.title.localeCompare(a.title);
                    }
                    return a.title.localeCompare(b.title);
                });
                sortOrder.title = !sortOrder.title;
                break;
            case 'completed':
                tempToDoList.sort(function(a, b) {
                    if( sortOrder.completed ) {
                        return (a.completed === b.completed)? 0 :  a.completed ? -1 : 1;
                    }
                    return (a.completed === b.completed)? 1 :  a.completed ? -1 : 0;
                });
                sortOrder.completed = !sortOrder.completed;
                break;
        }
    }
    
    function renderTableHeader() {
        let table = document.getElementById("todo-table");
        // Create an empty <thead> element and add it to the table:
        let header = table.createTHead();
        let tableHeaderkeys = Object.keys(toDoList[0]);
        let row = header.insertRow(0);     
        let cell;
        for(let i = 0; i < tableHeaderkeys.length -1 ;i++) {
            // Insert a new cell (<td>) at the first position of the "new" <tr> element:
            cell = row.insertCell(i);
            cell.onclick= (event) => renderTable(null,tableHeaderkeys[i+1]);
            // Add some bold text in the new cell:
            cell.innerHTML = `<strong>${tableHeaderkeys[i+1]}</strong>`;
        }
    }

    function drawChart() {
        let completed = 0;
        tempToDoList.forEach((todo) => {
                if(todo.completed) completed++;
        });
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
        type: 'bar',
        data:{
        labels: ["Completed", "Incompleted"],
                datasets: [{
                    label: 'Percentage',
                    data: [( (completed/tempToDoList.length) *100 ),(((tempToDoList.length - completed)/tempToDoList.length) * 100 )],
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                       
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(255,99,132,1)'
                       
                    ],
                    borderWidth: 1
                }]
        },
            options: {
                scales: {
                    
                    yAxes: [{
                    ticks: {
                        min: 0,
                        max: 100,
                        callback: function(value){return value+ "%"}
                        },  
                        scaleLabel: {
                        display: true,
                        labelString: "Completion rate %"
                        }
                    }]
                }
            }
        });
    }

   
